export interface DataModel {
  data: number[];
  label: string;
}
