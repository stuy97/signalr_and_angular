import { ChatService } from './../../services/chat.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  public hubConnected = false;
  public senderName = '';
  public chatMessage = '';
  public hideName = false;

  constructor(public chatService: ChatService) { }

  ngOnInit() {
  }

  public openChat = () => {
    this.hubConnected = this.chatService.startConnection(this.hubConnected);
    this.chatService.recieveMessageListener();
    this.chatService.typingListener();
  }

  public sendMessage = () => {
    if (this.chatMessage !== '') {
      this.chatService.sendMessageToChat(this.senderName, this.chatMessage);
      this.chatMessage = '';
      this.chatService.isTyping = '';
      this.chatService.sendTypingEvent('');
    }
  }

  public typing = () => {
    if (this.chatMessage === '') {
      this.chatService.isTyping = '';
      this.chatService.sendTypingEvent('');
    } else {
      this.chatService.sendTypingEvent(this.senderName);
    }
  }

  public hideNameEvent = () => {
    if (this.senderName.trim() !== '') {
      this.hideName = true;
    }
  }
}
