import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { ChartService } from 'src/app/services/chart.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, OnDestroy {
  private signalRSubcription: Subscription;

  public chartOptions = {
    scaleShowVerticalLines: true,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  };

  public chartLabels: string[] = ['Real time data for the chart'];
  public chartType = 'bar';
  public chartLegend = true;
  public colors: any[] = [
    {
      backgroundColor: '#5491DA'
    },
    {
      backgroundColor: '#E74C3C'
    },
    {
      backgroundColor: '#82E0AA'
    },
    {
      backgroundColor: '#E5E7E9'
    }
  ];

  constructor(public chartService: ChartService, private httpClient: HttpClient) { }

  ngOnInit() {
    this.chartService.startConnection();
    this.chartService.addTransferChartDataListener();
    this.chartService.addBroadcastChartDataListener();
  }

  public getDataFromService = () => {
    this.startHttpRequest();
  }

  ngOnDestroy(): void {
    if (this.signalRSubcription) {
      this.signalRSubcription.unsubscribe();
    }
  }

  private startHttpRequest = () => {
    this.signalRSubcription = this.httpClient.get('https://localhost:5001/api/chart').subscribe((res) => {
      console.log(res);
    });
  }

  public chartClicked = (e) => {
    console.log(e);
    this.chartService.broadcastChartData();
  }

}
