import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { MessageModel } from '../models/message-model';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private hubConnection: signalR.HubConnection;
  public chatMessages: MessageModel[] = [];
  public isTyping = '';

  public startConnection = (buttonClicked: boolean): boolean => {
    if (!buttonClicked) {
      this.hubConnection = new signalR.HubConnectionBuilder()
        .withUrl('https://localhost:5001/chat')
        .build();
      this.hubConnection.start()
        .then( () => {
          console.log('Connection started');
          return true;
        })
        .catch( (error) => {
          console.log(error);
          return false;
        });
    }
    return true;
  }

  public sendMessageToChat = (sender: string, message: string) => {
    this.hubConnection.invoke('sendMessage', sender, message)
    .catch((error) => console.log(error));
  }

  public recieveMessageListener = () => {
    this.hubConnection.on('receiveMessage',
      (resp) => {
        this.chatMessages.push(resp);
      }
    );
  }

  public sendTypingEvent = (senderName: string) => {
    const message = senderName !== '' ? senderName + ' is typing' : '';
    this.hubConnection.invoke('typingMessage', message)
    .catch((error) => console.log(error));
  }

  public typingListener = () => {
    this.hubConnection.on('typingMessage',
      resp => {
        this.isTyping = resp;
      }
    );
  }
}
