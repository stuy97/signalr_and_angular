import { Injectable } from '@angular/core';
import { ChartModel } from './../Models/chart-model';
import * as signalR from '@aspnet/signalr';

@Injectable({
  providedIn: 'root'
})
export class ChartService {
  public data: ChartModel[];
  public bradcastedData: ChartModel[];

  private hubConnection: signalR.HubConnection;

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('https://localhost:5001/chart')
      .build();
    this.hubConnection.start()
      .then( () => console.log('Connection started') )
      .catch( (error) => console.log(error) );
  }

  public addTransferChartDataListener = () => {
    this.hubConnection.on('transferChartData', (resp) => {
        this.data = resp;
        console.log(resp);
      }
    );
  }

  public broadcastChartData = () => {
    const newArray = [];
    this.data.forEach((value) => {
      newArray.push(
        {
          data: value.data,
          label: value.label,
        }
      );
    });
    console.log(newArray);
    this.hubConnection.invoke('broadcastChartData', newArray)
    .catch((error) => console.log(error));
  }

  public addBroadcastChartDataListener = () => {
    this.hubConnection.on('broadcastChartData',
      (resp) => {
        this.bradcastedData = resp;
      }
    );
  }
}
