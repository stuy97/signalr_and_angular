﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_First_Example.Models
{
    public class MessageModel
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }
}
