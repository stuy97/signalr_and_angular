﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using SignalR_First_Example.DataStorage;
using SignalR_First_Example.HubConfig;
using SignalR_First_Example.TimerFeatures;

namespace SignalR_First_Example.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChartController : ControllerBase
    {
        private readonly IHubContext<ChartHub> _hub;
        private static int counter = 0;

        public ChartController(IHubContext<ChartHub> hub)
        {
            _hub = hub;
        }

        [HttpGet]
        public IActionResult GetAction()
        {
            UpdateCounter();

            if (counter == 1)
            {
                var timerManager = new TimerManager(() => _hub.Clients.All.SendAsync("transferChartData", DataManager.GetData()));
                return Ok(new { Message = "Request Completed" });
            }

            return Ok(new { Message = "Is running" });
        }

        public static void UpdateCounter()
        {
            counter++;
        }

    }
}