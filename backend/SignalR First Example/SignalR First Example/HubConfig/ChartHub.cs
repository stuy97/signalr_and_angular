﻿using Microsoft.AspNetCore.SignalR;
using SignalR_First_Example.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_First_Example.HubConfig
{
    public class ChartHub : Hub
    {
        public async Task BroadcastChartData(List<ChartModel> data) => await Clients.Caller.SendAsync("broadcastChartData", data);
    }
}
