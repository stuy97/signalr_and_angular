﻿using Microsoft.AspNetCore.SignalR;
using SignalR_First_Example.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_First_Example.HubConfig
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string sender, string message)
        {
            var newMessage = new MessageModel
            {
                Name = sender,
                Message = message
            };
            await Clients.All.SendAsync("receiveMessage", newMessage);
        }

        public async Task TypingMessage(string message) => await Clients.All.SendAsync("typingMessage", message);

    }
}
