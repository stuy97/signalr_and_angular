﻿using SignalR_First_Example.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR_First_Example.DataStorage
{
    public class DataManager
    {
        public static List<ChartModel> GetData()
        {
            var rand = new Random();
            return new List<ChartModel>()
            {
                new ChartModel {
                    Data = new List<int> { rand.Next(1,40) },
                    Label = "Data 1"
                },
                new ChartModel { 
                    Data = new List<int> { rand.Next(1,40) },
                    Label = "Data 2"
                },
                new ChartModel { 
                    Data = new List<int> { rand.Next(1,40) },
                    Label = "Data 3"
                },
                new ChartModel {
                    Data = new List<int> { rand.Next(1,40) },
                    Label = "Data 4"
                }
            };
        }
    }
}
